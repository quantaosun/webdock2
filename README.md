# webdock2


## Test and Deploy

Hosted in in GitLab, deployed in Binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/quantaosun%2Fwebdock2/HEAD?labpath=webdock_v.0.0.2.ipynb )


## Description

Make docking easier, no registration, no login, and always free.


## Installation

No Installation required, use directly from an internect link in your browser.

## Usage

Click the link to lauch a temporary Binder session, then provide your protein and small molecule strings, click run > run all

## Support

No support, use at your own risk. To speed up the loading process, this docking procedure has been set to be a light and easy one, it might not be a proper docking in all cases. It may fail if the small molecule has too many rotatable bonds, or if your internect connection is slow, or if multiple users try to use this at the same time. Please retry if failed at a later time.

## Roadmap

On the fly 3D visualisation with all interactions displayed, wanted.

## Contributing

Please provide a proper ipynb file by attaching it into the issue button.

## Authors and acknowledgment
All the refernces are inside the notebook already, all open-sourced.

## License
For open source projects, for non-profit use only (MIT).

## Project status

It is  a casual project for myself and anyone might need it. It may will be changed at any time or remain just like this.
